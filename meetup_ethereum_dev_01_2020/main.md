---
marp: true
paginate: true
theme: default
---

<!-- footer: <img src="img/twitter.png">@vocdoni | ethereum dev BCN -->
<!-- page_number: true -->

# Architecture overview

![](img/vocdoni_banner.png)


---

# 1. Vocdoni platform components

![](img/vocdoni_family.png)

---

## Ethereum

Resilent and secure **source of truth**. Data integrity reference.

+ Entry point for any other component
+ Manage basic organization metadata
+ Publish election metadata
+ Publish election results

---

## IPFS

Distributed filesystem

+ Store organization data (logo, news feed, etc)
+ Publish election data (candidates, programs, etc)
+ Publish election census

---

## Vochain

Tendermint based custom blockchain

+ Very fast (~3s block time) Federated Proof-of-Authority
+ No tokens nor gas involved
+ Mainly used for counting votes during an election
+ Genesis information managed on Ethereum (as source of truth)
  + network id and epoch
  + miners and oracles public keys
+ Zk-snark validator integrated 

---

## Swarm/PSS

Kademlia DHT used by some components to distribute protocol messages

+ Gateways use it to publish themselves to bootnodes
+ Bootnodes use it to make a list of available Gateways
+ Used by the **ipfs-sync** (similar to ipfs-cluster)

---

## Gateways

Connects the Mobile APP with the rest of components

+ Websocket endpoint
+ IPFS node
+ Ethereum node
+ Vochain node
+ Swarm/PSS node

---

## Oracle

Vochain special identity that bridges Ethereum and Tendermint

+ Trusted set of cryptographic identities
+ Follows the Vocdoni's Smart Contract on Ethereum
+ When new election metadata available on Ethereum, send a TX to the Vochain
+ When an election is finished, compute the results and send a TX to Ethereum


---

![](img/architecture-main.png)


---

# Vocdoni's voting process overview

![](img/universallyVerifiable.png)

---

## Step 1: create the Census

+ **Census**: list of users that can participate in a governance process
+ Each organization maintains a list of public keys from their potential voters
+ Either in a database or in a public ledger
+ An organization can maintain multiple census

---

## Step 2: prepare the Census

When an election starts, the organization creates a merkle tree
![](img/merkle-tree.png)

---

## Step 3: export the Census

The organization exports the serialized merkle-tree to IPFS

<table border="0">
<tr>
<td>

![](img/ipfs.png)

</td>
<td>
 
 + **root**: 0x123... 
 + **uri**: ipfs://Q12345678...

</td>
</tr>
</table>

---
## Step 4: publish the election

The organization publish the election on the Vocdoni's process Smart Contract


<table border="0">
<tr>
<td>

![](img/ethereum.png)

</td>
<td>

+ **root**: 0x123...
+ **uri**: ipfs://Q12345678...
+ **type**: snark | poll | ...
+ **metadata**: ipfs://Q666777...
+ **start**: block 10000
+ **duration**: 1024 blocks
+ ...

</td>
</tr>
</table>

---

## Step 5: election warm-up

The Vocdoni compoments which are following the Ethereum smart contract:

+ **Gateways**: download and import the census to a local database
+ **Oracle**: sends a `newProcessTx` transaction to the Tendermint Vochain
+ **Vochain**: miners include the `newProcessTx` to the blockchain, the process will be open from block 10000 until block 11024
+ **Miners**: if the election is private, generate the temporary keys to encrypt the vote choice data

---

## Step 6: users warm-up

The **APP users** registered to the organization will see the new election

+ using any available Gateway, check if they are in the **census**
+ fetch the **merkle proof** from the Gateway
+ fetch the **metadata** to see the available voting options
+ fetch the **encryption public keys** if the election is private
+ see when **election starts** in UTC time

---

## Step 7: election starts

Elegible APP users now can vote

+ make the **vote choice** summarized in a integer array 
  + `[ 1, 3, 1]` => question1:option1, question2:option3, question3:option1
+ if the election is private, **encrypt** the vote choice with the temporary keys
+ compute the zero **knowledge proof** using the merkle proof and private key
  + `privateKey+MerkleProof` => zk-proof
+ pack everything and send it to one or several **Gateways**
+ save the `nullifier` so the user can identify its vote afterwars

---

## Setp 8: mine the transaction

The Gateway(s) will broadcast the vote transaction to the Tendermint Vochain mempool

+ the Vochain miners will accept the transaction if:
  + the election process is alive
  + the zk-proof is valid
  + the nullifier has not been already used (in case of uniq vote election)

---

## Setp 9: election ends

The Vochain mines block 11024 so no more votes are allowed

+ if election is private, miners **publish the temporary private keys**
+ anyone can decrypt the votes and **compute the results**
+ a user can check, using the **nullifier** if its vote has been counted
+ Gateways will compute the results and let them available for APP users
+ The Oracle will send a transaction with the results to the Ethereum Smart Contract for the record 

---

# Vocdoni's Zk-snarks circuit

---

## Circuit Inputs/Outputs

+ **Private inputs**: Private Key, Census Merkle-proof, Vote-signature
+ **Public inputs**: Census Merkle-root, Nullifier, ProcessId, Vote
+ **Output**: Franchise proof

_nullifier = hash( process_id + user_private_key )_

---

![](img/zksnarks.png)

---

# Stay in touch

+ web: https://vocdoni.io
+ docs: https://vocdoni.io/docs
+ repository: https://gitlab.com/vocdoni
+ twitter: @vocdoni
+ telegram: @vocdoni

We are hiring! e-mail to hello@vocdoni.io

Support us on GitCoin https://gitcoin.co/grants