---
marp: true
paginate: true
theme: default
---

<!-- footer: vocdoni.io -->
<!-- page_number: true -->


![](img/vocdoni_banner.png)


---

# Current e-Voting schema
https://courses.cs.ut.ee/2015/infsec/fall/Main/E-voting

![width:780px](img/estonian_evoting.png)

---

##### Basic operation for most of e-Voting schemas

1. The user encrypts and signs the vote
2. The first server verify and removes the signature
3. The second server decrypts the vote
4. The vote is tallied

##### WTF?
1. **not anonymous**: someone controlling both servers might know who voted what
2. **not e2e verifiable**: a user cannot check its vote was correctly counted
3. **weak and centralized**: it is easy to attack

---

# Vocdoni's proposal

![](img/estonian_evoting2.png)

---

## decentralized voting or d-Voting

+ using p2p technologies
+ using end-to-end cryptography
+ using self sovereign identity schema
+ using permision-less components: no need to trust anyone

**everything Open Source!**


---

![](img/distributed_backend.png)

---

# Ethereum

Resilent and secure **source of truth**. Data integrity reference.

+ Entry point for any other component
+ Manage basic organization metadata
+ Publish election metadata
+ Publish election results

---

# IPFS

Distributed filesystem

+ Store organization data (logo, news feed, etc)
+ Publish election data (candidates, programs, etc)
+ Publish election census

---

# Vochain

Tendermint based custom blockchain

+ Very fast (~5s block time) Federated Proof-of-Authority
+ No tokens nor gas involved
+ Mainly used for counting votes during an election
+ Genesis information managed on Ethereum (as source of truth)
  + network id and epoch
  + miners and oracles public keys
+ Zk-snark validator integrated 

---

# libp2p

p2p DHT used by some components to distribute protocol messages

+ Gateways use it to announce themselves to bootnodes
+ Bootnodes use it to make a list of available Gateways
+ Used by the **ipfs-sync** component (similar to ipfs-cluster)

---

# The census

+ **Census**: list of users that can participate in a governance process
+ Each organization maintains a list of public keys from their potential voters

![width:800px](img/merkle-tree.png)


---

![bg contain](img/voting_scheme.png)

---

# Privacy based on zk-SNARKs

+ **Private inputs**: Private Key, Census Merkle-proof, Vote-signature
+ **Public inputs**: Census Merkle-root, Nullifier, ProcessId, Vote
+ **Output**: ZK-proof

_nullifier = hash( process_id + user_private_key )_

---

![](img/zksnarks.png)

---

# Stay in touch

+ web: https://vocdoni.io
+ docs: https://vocdoni.io/docs
+ repository: https://gitlab.com/vocdoni
+ twitter: @vocdoni
+ telegram: @vocdoni

We are hiring! e-mail to hello@vocdoni.io
